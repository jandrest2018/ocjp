package com.primerdeber;

/**
 * Created by Black on 5/5/2017.
 */
public class Resta extends Operacion {

    private double restar;

    protected Resta(double numero1, double numero2){
        super(numero1,numero2,'-');
        this.restar = numero1 - numero2;
        this.setResultado(this.restar);
    }
}
