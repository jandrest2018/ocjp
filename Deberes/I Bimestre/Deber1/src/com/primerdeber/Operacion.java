package com.primerdeber;

/**
 * Created by Black on 4/5/2017.
 */
public class Operacion {
    private double numero1;
    private double numero2;
    private double resultado;
    private char operacion;

    public Operacion(double numero1, double numero2, char operacion){
         this.numero1 = numero1;
         this.numero2 = numero2;
         this.operacion = operacion;
    }

    public void mostrarResultado(){
        System.out.print("El resutlado de la operación "+operacion+" es :");
        System.out.println(this.numero1+" "+this.operacion+" "+this.numero2+" = "+this.resultado);
    }

    public double getNumero1(){
        return numero1;
    }
    public void setNumero1(double numero1){
        this.numero1 = numero1;
    }


    public double getNumero2(){
        return numero2;
    }
    public void setNumero2(double numero2){
        this.numero2 = numero2;
    }


    public char getOperacion(){
        return operacion;
    }
    public void setOperacion(char operacion){
        this.operacion = operacion;
    }


    public double getResultado(){
        return resultado;
    }
    public void setResultado(double resultado){
        this.resultado = resultado;
    }
}
