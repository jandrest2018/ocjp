package com.primerdeber;

/**
 * Created by Black on 5/5/2017.
 */
public class Division extends Operacion {

    private double dividir;

    protected Division(double numero1, double numero2){
        super(numero1, numero2, '/');
        if(numero2 == 0)
            System.out.println("No se puede hacer división para cero");
        else
            this.dividir = numero1 / numero2;
        this.setResultado(this.dividir);
    }
}
