package com.primerdeber;

/**
 * Created by Black on 5/5/2017.
 */
public class Multiplicacion extends Operacion {

    private double multiplicar;

    protected Multiplicacion(double numero1, double numero2){
        super(numero1, numero2, '*');
        this.multiplicar = numero1 * numero2;
        this.setResultado(this.multiplicar);
    }
}
