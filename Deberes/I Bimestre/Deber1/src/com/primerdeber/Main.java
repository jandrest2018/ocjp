package com.primerdeber;

import com.sun.org.apache.regexp.internal.RE;
import java.io.IOException;
import java.io.*;
import java.util.Scanner;

/**
 * Created by Black on 4/5/2017.
 */
public class Main {
    public static void main (String[] args){
        //write your code here
        System.out.println("Bienvenidos a Calculadora ^_^\n\n");

        double numero1 = 0;
        double numero2 = 0;

        //Lectura desde teclado del primer numero
        System.out.println("Ingrese el primer número");
        Scanner leer1 = new Scanner(System.in);
        numero1 = leer1.nextDouble();

        //Lectura desde teclado del segundo numero
        Scanner leer2 = new Scanner(System.in);
        System.out.println("Ingrese el segundo número");
        numero2 = leer2.nextDouble();


        //Suma
        Suma sumar = new Suma(numero1,numero2);
        sumar.mostrarResultado();

        //Resta
        Resta restar = new Resta(numero1,numero2);
        restar.mostrarResultado();

        //Multiplicacion
        Multiplicacion multiplicar = new Multiplicacion(numero1,numero2);
        multiplicar.mostrarResultado();

        //Dividir
        Division dividir = new Division(numero1,numero2);
        dividir.mostrarResultado();

        //Modulo
        Modulo residuo = new Modulo(numero1,numero2);
        residuo.mostrarResultado();

    }
}