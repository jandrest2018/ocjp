package com.primerdeber;

/**
 * Created by Black on 7/5/2017.
 */
public class Modulo extends Operacion {

    private double residuo;

    protected Modulo(double numero1, double numero2){
        super(numero1, numero2, '%');
        if(numero2 == 0)
            System.out.println("No se puede hacer división para cero");
        else
            this.residuo = numero1 % numero2;
        this.setResultado(this.residuo);
    }
}
