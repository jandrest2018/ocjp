package com.primerdeber;

/**
 * Created by Black on 4/5/2017.
 */
public class Suma extends Operacion {

    private double sumar;

    protected Suma(double numero1, double numero2){
        super(numero1, numero2, '+');
        this.sumar = numero1 + numero2;
        this.setResultado(this.sumar);
    }
}
