package deberocjp;
import java.util.Scanner;

/**
 * Created by Black on 15/5/2017.
 */
public class Persona {

    String nombre;
    private String apellido;
    protected int edad;

    //CONSTRUCTOR
    public Persona(){
        nombre = "";
        edad = 0;
    }

    //---------------SobreCarga---------------
    //Metodo Ingreso de datos por consola
    public void ingresar_datos_persona(){

        //Ingreso Nombre
        System.out.print("Ingrese Nombre : ");
        Scanner leer_nombre = new Scanner(System.in);
        nombre = leer_nombre.next();

        //Ingreso Apellido
        System.out.print("Ingrese Apellido : ");
        Scanner leer_apellido = new Scanner(System.in);
        apellido = leer_apellido.next();

        //Ingreso Edad
        System.out.print("Ingrese Edad : ");
        Scanner leer_edad = new Scanner(System.in);
        edad = leer_edad.nextInt();
    }

    public  void ingresar_datos_persona(String nombre){
        //Ingreso Nombre
        System.out.print("Ingrese Nombre : ");
        Scanner leer_nombre = new Scanner(System.in);
        nombre = leer_nombre.next();

        //Ingreso Apellido
        System.out.print("Ingrese Apellido : ");
        Scanner leer_apellido = new Scanner(System.in);
        apellido = leer_apellido.next();

        //Ingreso Edad
        System.out.print("Ingrese Edad : ");
        Scanner leer_edad = new Scanner(System.in);
        edad = leer_edad.nextInt();
    }

    public  void ingresar_datos_persona(String nombre, String apellido){
        //Ingreso Nombre
        System.out.print("Ingrese Nombre : ");
        Scanner leer_nombre = new Scanner(System.in);
        nombre = leer_nombre.next();

        //Ingreso Apellido
        System.out.print("Ingrese Apellido : ");
        Scanner leer_apellido = new Scanner(System.in);
        apellido = leer_apellido.next();

        //Ingreso Edad
        System.out.print("Ingrese Edad : ");
        Scanner leer_edad = new Scanner(System.in);
        edad = leer_edad.nextInt();
    }

    public  void ingresar_datos_persona(String nombre, String apellido, int Edad){
        //Ingreso Nombre
        System.out.print("Ingrese Nombre : ");
        Scanner leer_nombre = new Scanner(System.in);
        nombre = leer_nombre.next();

        //Ingreso Apellido
        System.out.print("Ingrese Apellido : ");
        Scanner leer_apellido = new Scanner(System.in);
        apellido = leer_apellido.next();

        //Ingreso Edad
        System.out.print("Ingrese Edad : ");
        Scanner leer_edad = new Scanner(System.in);
        edad = leer_edad.nextInt();
    }
    //---------------SobreCarga---------------


    //Metodo Impresion de datos
    public void printToConsole() {
        System.out.println("\nEl nombre es: "+nombre);
        System.out.println("El apellido es : " +apellido);
        System.out.println("La edad es : " +edad);
    }

}
