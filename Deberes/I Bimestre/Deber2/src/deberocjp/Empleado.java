package deberocjp;
import java.util.Scanner;

/**
 * Created by Black on 15/5/2017.
 */
public class Empleado extends Persona {
    protected String cargo;
    int sueldo;

    //Constructor Vacio
    public Empleado() {
    }

    //Metodo Ingreso de datos por consola
    void ingresar_datos_empleado(){
        System.out.print("Ingrese Cargo : ");
        Scanner leer_cargo = new Scanner(System.in);
        cargo = leer_cargo.next();

        System.out.print("Ingrese Sueldo : ");
        Scanner leer_sueldo = new Scanner(System.in);
        sueldo = leer_sueldo.nextInt();
    }

    //Metodo heredado Impresion de datos
    @Override
    public void printToConsole() {
        System.out.println("El cargo es : "+cargo);
        System.out.println("El sueldo es : "+sueldo);
    }
}
