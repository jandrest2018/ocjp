package deberocjp;

/**
 * Created by Black on 15/5/2017.
 */
public class main {

    public static void main(String[] args){

        //Escuela Politecnica Nacional --- DEBER #2 --- José Andrés Tinajero
        System.out.println("DEBER #2 DE OCJP");

        //llamada al metodo ingresar datos de persona
        Persona datos_persona = new Persona();
        datos_persona.ingresar_datos_persona();

        //llamada al metodo ingresar datos de empleado
        Empleado datos_empleado = new Empleado();
        datos_empleado.ingresar_datos_empleado();

        //llamada al metodo imprimir datos de persona y empleado
        datos_persona.printToConsole();
        datos_empleado.printToConsole();


        ContratoEmpleado estado_contrato_empleado = new EstadoContratoEmpleado();
        if(datos_empleado.sueldo >= 500){
            estado_contrato_empleado.contratado();
        } else if (datos_empleado.sueldo == 0){
            estado_contrato_empleado.despedido();
        } else {
            estado_contrato_empleado.aPrueba();
        }

        
        AccionEmpleado estado_accion;
        estado_accion = AccionEmpleado.TRABAJANDO;

        switch (estado_accion){
            case TRABAJANDO:
                System.out.println("El Empleado esta TRABAJANDO");
                break;
            case DESCANSANDO:
                System.out.println("El Empleado esta DESCANSANDO");
                break;
            default:
                System.out.println("Error");
            }
    }
}
