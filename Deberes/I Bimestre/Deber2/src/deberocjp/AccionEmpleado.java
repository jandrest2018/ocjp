package deberocjp;

/**
 * Created by Black on 15/5/2017.
 */
public enum AccionEmpleado {
    TRABAJANDO(),DESCANSANDO();

    private String estado_accion_empleado;
    private int i = 0;

    AccionEmpleado() {
    }

    private AccionEmpleado(AccionEmpleado estado_accion_Empleado){
        this.estado_accion_empleado = estado_accion_empleado;
    }

    public AccionEmpleado siguiente(boolean ocupado){
        if (ocupado){
            return TRABAJANDO;
        }else{
            return DESCANSANDO;
        }
    }
}
