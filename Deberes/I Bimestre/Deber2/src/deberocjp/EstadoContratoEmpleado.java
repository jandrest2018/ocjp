package deberocjp;

/**
 * Created by Black on 16/5/2017.
 */
public class EstadoContratoEmpleado implements ContratoEmpleado {

    @Override
    public void aPrueba() {
        System.out.println("El Empleado esta A PRUEBA");
    }

    @Override
    public void contratado() {
        System.out.println("El Empleado esta CONTRATADO");
    }

    @Override
    public void despedido() {
        System.out.println("El Empleado esta DESPEDIDO");
    }
}
