package deberocjp;

/**
 * Created by Black on 2/6/2017.
 */
public class main {

    public static void main(String[] args){
        //Escuela Politecnica Nacional --- DEBER #3 --- José Andrés Tinajero
        System.out.println("--------------DEBER #3 DE OCJP--------------");

        //Impresion Tipo de Datos Primitivos
        Primitivos imprimir_primitivos = new Primitivos();
        imprimir_primitivos.imprimir_datos_primitivos();

        //Impresion Tipo de Datos No Primitivos
        NoPrimitivos imprimir_no_primitivos = new NoPrimitivos();
        imprimir_no_primitivos.imprimir_datos_no_primitivos();

        //Impresion de Operadores
        Operadores operaciones = new Operadores();
        operaciones.imprimir_numeros();
        operaciones.setSuma();
        operaciones.setResta();
        operaciones.setMultiplicación();
        operaciones.setDivisión();
        operaciones.setModulo();
        operaciones.setSumaSuma();
        operaciones.setRestaResta();
        operaciones.setBoolean1();
        operaciones.setBoolean2();
        operaciones.setBoolean3();

        //Impresion de datos convertidos
        Conversiones convertir_tipo_datos = new Conversiones();
        convertir_tipo_datos.imprimir_datos_convetidos();
        
    }
}
