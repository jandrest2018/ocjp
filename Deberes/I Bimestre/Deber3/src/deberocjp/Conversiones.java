package deberocjp;

/**
 * Created by Black on 5/6/2017.
 */
public class Conversiones extends Primitivos {

    protected float tipo_float = (float) (200*tipo_int/tipo_double + 5);
    void imprimir_datos_convetidos() {
        System.out.println("\n---CONVERSION DE TIPO DE DATOS---");
        System.out.println("Primer numero : "+tipo_int);
        System.out.println("Segundo numero : "+tipo_double);
        System.out.println("Resultado en formato Byte: " + (byte) tipo_float);
        System.out.println("Resultado en formato Short: " + (short) tipo_float);
        System.out.println("Resultado en formato Int: " + (int) tipo_float);
        System.out.println("Resultado en formato Long: " + (long) tipo_float);
        System.out.println("Resultado en formato Float: " + tipo_float);
        System.out.println("Resultado en formato Double: " + (double) tipo_float);
    }
}
