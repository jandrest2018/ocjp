package deberocjp;

/**
 * Created by Black on 5/6/2017.
 */
public class Operadores extends Primitivos {


    double numero1 = tipo_short;
    double numero2 = tipo_float;
    double suma;
    double resta;
    double multiplicación;
    double division;
    double modulo;

    public void imprimir_numeros(){
        System.out.println("\n---EJEMPLOS DE TIPOS DE OPERADORES---");
        System.out.println("El primer numero es : "+numero1);
        System.out.println("El segundo numero es : "+numero2);
    }

    public void setSuma() {
        this.suma = numero1 + numero2;
        System.out.println("EL resultado de la operación + es: "+suma);
    }

    public void setResta() {
        this.resta = numero1 - numero2;
        System.out.println("EL resultado de la operación - es: "+resta);
    }

    public void setMultiplicación() {
        this.multiplicación = numero1 * numero2;
        System.out.println("EL resultado de la operación * es: "+multiplicación);
    }

    public void setDivisión() {
        this.division = numero1 / numero2;
        System.out.println("EL resultado de la operación / es: "+division);
    }

    public void setModulo() {
        this.modulo = numero1 % numero2;
        System.out.println("EL resultado de la operación % es: "+modulo);
    }

    public void setSumaSuma(){
        this.numero1 = numero2++;
        System.out.print("\nDespues de la operacion y=x++, el primer numero es: "+numero1);
        System.out.println(" y el segundo numero es: "+numero2);
    }

    public void setRestaResta(){
        this.numero1 = numero2--;
        System.out.print("Despues de la operacion y=x--, el primer numero es: "+numero1);
        System.out.println(" y el segundo numero es: "+numero2);
    }

    public void setBoolean1(){
        System.out.println("\nValor anterior del dato Boolean es: "+ tipo_boolean1);
        this.tipo_boolean1 = !tipo_boolean1;
        System.out.println("Después de la operacion y=!y del dato Boolean es: "+ tipo_boolean1);
    }

    public void setBoolean2(){
        System.out.println("\nValor Primer dato Boolean es: "+ tipo_boolean1);
        System.out.println("Valor Segundo dato Boolean es: "+ tipo_boolean2);
        if (tipo_boolean1 && tipo_boolean2){
            this.tipo_boolean1 = true;
        }
        else {
            this.tipo_boolean1 = false;
        }
        System.out.println("Después de la operacion x&&y del Primer dato Boolean es: "+ tipo_boolean1);
        System.out.println("Y del Segundo dato Boolean es: "+tipo_boolean2);
    }

    public void setBoolean3(){
        System.out.println("\nValor Primer dato Boolean es: "+ tipo_boolean1);
        System.out.println("Valor Segundo dato Boolean es: "+ tipo_boolean2);
        if (tipo_boolean1 || tipo_boolean2){
            this.tipo_boolean1 = false;
        }
        else {
            this.tipo_boolean1 = true;
        }
        System.out.println("Después de la operacion x||y del Primer dato Boolean es: "+ tipo_boolean1);
        System.out.println("Y del Segundo dato Boolean es: "+tipo_boolean2);
    }
}
