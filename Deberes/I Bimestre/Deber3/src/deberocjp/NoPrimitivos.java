package deberocjp;
import java.util.Scanner;

/**
 * Created by Black on 5/6/2017.
 */
public class NoPrimitivos {

    public String tipo_string = "Soy un String";
    Integer tipo_integer = null;

    void imprimir_datos_no_primitivos() {
        System.out.println("\n---EJEMPLOS DE TIPOS DE DATOS NO PRIMITIVOS---");
        System.out.println("Tipo de dato Char : " + tipo_string);
        System.out.println("Tipo de dato Integer : "+tipo_integer);
    }
}
