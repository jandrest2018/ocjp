package deberocjp;

/**
 * Created by Black on 5/6/2017.
 */
public class Primitivos {

    char tipo_char = 'j';

    byte tipo_byte = 122;
    short tipo_short = -29000;
    int tipo_int = 1000010;
    long tipo_long = 999999999999L;

    float tipo_float = 234.99F;
    double tipo_double = 55100.5;

    boolean tipo_boolean1 = false;
    boolean tipo_boolean2 = false;


    void imprimir_datos_primitivos(){
        System.out.println("\n---EJEMPLOS DE TIPOS DE DATOS PRIMITIVOS---");
        System.out.println("Tipo de dato Char : "+tipo_char);
        System.out.println("Tipo de dato Byte : "+tipo_byte);
        System.out.println("Tipo de dato Short : "+tipo_short);
        System.out.println("Tipo de dato Int : "+tipo_int);
        System.out.println("Tipo de dato Long : "+tipo_long);
        System.out.println("Tipo de dato Float : "+tipo_float);
        System.out.println("Tipo de dato Double : "+tipo_double);
        System.out.println("Tipo de dato Boolean : "+ tipo_boolean1);
    }

}
