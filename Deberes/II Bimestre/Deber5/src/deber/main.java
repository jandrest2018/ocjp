package deber;

import java.util.*;
import java.util.Scanner;

/**
 * Created by Black on 3/7/2017.
 */
public class main {
    public static void main(String[] args){
        //Escuela Politecnica Nacional --- DEBER #5 --- José Andrés Tinajero
        System.out.println("DEBER #5 DE OCJP\n");

        //Definicion de Variables
        String texto="";
        String textoinvertido="";
        String textosinespacios="";

        //Ingreso de palabra por teclado
        System.out.println("Ingresa Una Palabra que sea un Palindromo");
        Scanner entrada = new Scanner(System.in);
        texto=entrada.nextLine();

        //Recorrido letra por letra, izquierda a derecha
        for (int x=texto.length()-1;x >=0; x--)
        textoinvertido = textoinvertido + texto.charAt(x);

        //Verificacion que mi palabra es un palindromo
        if (texto.equalsIgnoreCase(textoinvertido)) {
            System.out.println(texto + "\nSI es palindromo");
        }
        else {
            System.out.println(texto + "\nNO es palindromo");
        }
    }
}
