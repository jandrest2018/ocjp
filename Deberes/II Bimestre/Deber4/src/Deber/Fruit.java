package Deber;

/**
 * Created by Black on 26/6/2017.
 */
public class Fruit {
    public static void main(String[] agrs){

        //Cast clase manzana a interface squee
        Apple manzana = new Apple();

        try{
            Squeezable squee = (Squeezable) manzana;
            System.out.println("OK");
        }
        catch (Exception e){
            System.out.println("NO");
        }

        try{
            Squeezable squee = (Squeezable) manzana;
            Apple manzana2 = (Apple) squee;
            System.out.println("OK");
        }
        catch (Exception e){
            System.out.println("NO");
        }


        //Cast arreglo de Citrus a un arreglo de Fruit.
        Fruit fruits[];
        Orange oranges[];
        Citrus citrues[] = new Citrus[10];
        for (int i = 0; i < 10; i++){
            citrues[i] = new Citrus();
        }
        fruits = citrues;
    }

}
