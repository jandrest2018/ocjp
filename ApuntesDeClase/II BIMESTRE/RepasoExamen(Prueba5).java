class A{
	static void m1(String s){
		s = s.trim();
		s = s.concat("D");
	}
	public static void main(String[] s){
		String s1 = "A";
		String s2 = " B ";
		String s3 = "C";
		m1(s2);
		System.out.print(s1 + s2 + s3);
	}
}
IMPRIME A B C


class E{
	public static void main(String[] args){
		String s1 = "ABCDE";
		System.out.print(s1.substring(1,2)+s1.substring(3));
	}
}
IMPRIME BDE


class J{
    public static void main(String[] args){
        StringBuffer s1 = new StringBuffer(32);
        System.out.print(s1.length()+","+s1.capacity());
    }
}
IMPRIME 0,32


class A{
	public static void main(String[] s){
		String s1 = "ABC";
		String s2 = new String(s1);
		String s3 = new String(s1);
		System.out.print((s2 == s3)+",");
		System.out.print((s2.trim() == s2.toUpperCase())+",");
		System.out.print(s3.concat("") == s3.replace('D','E'));
	}
}
IMPRIME false,true,true


Map: -Entradas son organizadas como llave/valor.
	 -Entradas duplicadas reemplazan el valor de la entrada antigua.

	 
TreeMap: -Guarda pares de llave/valor
		 -Acepta elementos null, llaves, valores
		 -Entradas duplicadas reemplazan los valores antiguos
		 -Entradas son ordenadas implementando la interface Comparable


import java.util.*;
public class Apple{
	public static void main(String[] a){
		Set<Apple> set = new TreeSet<Apple>();
		set.add(new Apple()); //62
		set.add(new Apple());
		set.add(new Apple());
	}
}
IMPRIME Error de compilación en la linea 62


class G{
	public static void main(String[] args){
		//1
	}
}
PONER para que exista errores Float f1 = new Float("A"); Float f1 = new Float("1L");


class A {
	public static void main(String args[]){
		Integer i1 = new Integer(1);
		Integer i2 = new Integer(i1);
		System.out.print(i1.equals(i2));
	}
}
IMPRIME true

En las siguientes no existe ningun error de compilación
-Short s1 = new Short("1");
-Short s1 = new Short("-1");
-Short s1 = new Short("1,0");
