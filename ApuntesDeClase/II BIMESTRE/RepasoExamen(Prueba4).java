public class AssertTest{
	public static void main(String args[]){
		int i = 5;
		Object obj1 = new Object();
		Object obj2 = obj1;
		
		try{
			assert obj1.equals(obj2):i+=10;
		}
		catch(AssertionError ignore){}
		System.out.println(i);
	}
}
IMPRIME 5


public class AssertTest{
	static int i = 10;
	public static void main(String args[]){
		i = i*2;
		try{
			assert isValid():i=i/4;
		}
		catch(AssertionError ignore){}
		System.out.println("i="+i);
	}
	
	public static boolean isValid(){
		i = i*2;
		return false;
	}
}
IMPRIME 10


class ColorException extends Exception {}
class WhiteException extends ColorException {}
class White{
    void m1() throws ColorException{throw new WhiteException();}
    void m2() throws WhiteException{}
    public static void main(String args[]){
        White white = new White();
        int a, b, d, f;
        a = b = d = f = 0;
        try{white.m1();a++;} catch(ColorException e){b++;}
        try{white.m2();d++;} catch(WhiteException e){f++;}
        System.out.print(a+","+b+","+d+","+f);
    }
}
IMPRIME 0,1,1,0


class ColorException extends Exception {}
class WhiteException extends ColorException{}
abstract class Color{
    abstract void m1() throws ColorException;
}
class White extends Color{
    void m1() throws WhiteException{throw new WhiteException();}
    public static void main(String[] args){
        White white = new White();
        int a, b , c, d;
        a = b = c = d = 0;
        try{
            white.m1();
            a++;
        } catch(WhiteException e){b++;}
        finally {c++;}
        System.out.print(a+","+b+","+c);
    }
}
IMPRIME 0,1,1


public class AssertDemo{
	private static boolean isValid(int i){
		return 10%i>0;
	}
	public static void main(String args[]){
		int i = 0;
		assert isValid(i):"Invalid value";
		System.out.println(i);
	}
}
IMPRIME Error en ejecución


class Level1Exception extends Exception {}
class Level2Exception extends Level1Exception {}
class Level3Exception extends Level2Exception {}
class Purple{
    public static void main(String args[]){
        int a, b, c, d, f, g, x;
        a = b = c = d = f = g = 0;
        x = 4;
        try{
            try{
                switch(x){
                    case 1: throw new Level1Exception();
                    case 2: throw new Level2Exception();
                    case 3: throw new Level2Exception();
                    case 4: throw new Exception();
                }
                a++;
            } catch (Level2Exception e){b++;}
            finally{c++;}
        }
        catch (Level1Exception e){d++;}
        catch (Exception e){f++;}
        finally {g++;}
        System.out.print(a+","+b+","+c+","+d+","+f+","+g);
    }
}
IMPRIME 0,0,1,0,1,1


class Level1Exception extends Exception {}
class Level2Exception extends Level1Exception {}
class Level3Exception extends Level2Exception {}
class Purple{
	public static void main(String args[]){
		int a, b, c, d, f, g, x;
		a = b = c = d = f = g = 0;
		x = 5;
		try{
			try{
				switch(x){
				case 1: throw new Level1Exception();
				case 2: throw new Level2Exception();
				case 3: throw new Level2Exception();
				case 4: throw new Exception();
				}
				a++;
			}
			catch (Level2Exception e){b++;}
			finally{c++;}
		}
		catch (Level1Exception e){d++;}
		catch (Exception e){f++;}
		finally{g++;}
		System.out.print(a+","+b+","+c+","+d+","+f+","+g);
	}
}
IMPRIME 1,0,1,0,0,1


class Level1Exception extends Exception {}
class Level2Exception extends Level1Exception {}
class Level3Exception extends Level2Exception {}
class Brown{
	public static void main(String args[]){
		int a, b, c, d, f;
		a = b = c = d = f = 0;
		int x =1;
		try{
			switch(x){
				case 1: throw new Level1Exception();
				case 2: throw new Level2Exception();
				case 3: throw new Level3Exception();
			}
			a++;
		}
		catch (Level3Exception e){b++;}
		catch (Level2Exception e){c++;}
		catch (Level1Exception e){d++;}
		finally{f++;}
		System.out.print(a+","+b+","+c+","+d+","+f);
	}
}
IMPRIME 0,0,0,1,1


public class AssertTest{
	public void methodA(int i){
		assert i>=0; methodB();
		System.out.println(i);
	}
	public void methodB(){
		System.out.println("The value must not be negative");
	}
	public static void main(String args[]){
		AssertTest test = new AssertTest();
		test.methodA(-10);
	}
}
IMPRIME -10


class ColorException extends Exception {}
class WhiteException extends ColorException {}
class White{
	void m1() throws ColorException {throw new ColorException();}
	void m2() throws WhiteException {throw new ColorException();}
	public static void main (String[] args){
		White white = new White();
		int a, b, d, f;
		a = b = d = f = 0;
		try {white.m1();a++;} catch (ColorException e){b++;}
		try {white.m2();d++;} catch (WhiteException e){f++;}
		System.out.print(a+","+b+","+d+","+f);
	}
}
IMPRIME Error en Compilación