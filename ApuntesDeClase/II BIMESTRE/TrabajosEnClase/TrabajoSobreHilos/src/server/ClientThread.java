package server;
import helper.JSystem;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.PrintStream;
import java.net.Socket;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;
import java.util.Random;
import helper.Helper;


public class ClientThread implements Runnable {


    //Socket para la comunicacion cliente servidor
    private Socket clientSocket;

    //identificador para el hilo
    private int id;

    //Color de fondo para texto en consola
    private JSystem.ColorBg colorThread;


    //Constructor de clase
    public ClientThread(Socket clientSocket, int id) {
        this.clientSocket = clientSocket;
        this.id = id;
        colorThread = bgColor();
    }

    @Override
    public void run() {
        try {
            //para imprimir datos de salida
            PrintStream output = new PrintStream(clientSocket.getOutputStream());

            JSystem.out.printColor(colorThread, JSystem.Color.white, "Server>");
            JSystem.out.printColorln(JSystem.Color.green, " Nueva petición aceptada [id: " + id + "] - " + Helper.fn.fecha_actual());

            //Para leer lo que envie el cliente
            BufferedReader input = new BufferedReader(new InputStreamReader(clientSocket.getInputStream()));

            //se lee peticion del cliente. En este caso es un numero entero
            int request = Integer.valueOf(input.readLine());
            JSystem.out.printColor(colorThread, JSystem.Color.white, "Server>");
            JSystem.out.printColorln(JSystem.Color.black, " Thread [id: " + id + "] procesando...");

            //se genera el fibonacci
            String f = fibo(request);

            //duerme el hilo unos segundos para emular procesos que pueden durar varios segundos o minutos,etc
            Helper.fn.sleep();

            //imprime en consola el resultado
            JSystem.out.printColor(colorThread, JSystem.Color.white, "Server>");
            JSystem.out.printColorln(JSystem.Color.black, " Cliente [id: " + id + "] fibonacci de " + request + " = " + f);

            //se imprime en cliente
            output.flush();//vacia contenido
            output.println(f);

            //cierra conexion
            clientSocket.close();

            //notifica muerte de hilo
            JSystem.out.printColor(colorThread, JSystem.Color.white, "Server>");
            JSystem.out.printColorln(JSystem.Color.red, " Thread [id: " + id + "] termina - " + Helper.fn.fecha_actual());
        } catch (IOException | NumberFormatException e) {
            System.err.println(e.getMessage());
        }
    }

    //Genera la sucesión de fibonacci
    private String fibo(int num) {
        int fibo1 = 1;
        int fibo2 = 1;
        int aux = 1;
        String cadena = "1";
        for (int i = 2; i <= num; i++) {
            fibo2 += aux;
            aux = fibo1;
            fibo1 = fibo2;
            cadena += " " + aux;
        }
        return cadena;
    }


    //genera un color aleatorio para el hilo
    private JSystem.ColorBg bgColor() {
        int rnd;
        List<JSystem.ColorBg> VALUES = Collections.unmodifiableList(Arrays.asList(JSystem.ColorBg.values()));
        int SIZE = VALUES.size();
        do {
            Random RANDOM = new Random();
            rnd = RANDOM.nextInt(SIZE);
        } while (VALUES.get(rnd)==JSystem.ColorBg.black || VALUES.get(rnd)==JSystem.ColorBg.white);
        return VALUES.get(rnd);
    }

}//end ClientThread