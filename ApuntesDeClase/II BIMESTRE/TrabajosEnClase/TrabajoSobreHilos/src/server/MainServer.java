package server;
import helper.JSystem;
import java.io.IOException;
import java.net.ServerSocket;
import java.net.Socket;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.ThreadPoolExecutor;

public class MainServer {


    //Puerto
    private final int PORT = 5000;

    //Identificador para Thread
    private int idThread = 0;

    //Para controlar la ejecución de los hilos

    private ThreadPoolExecutor pool;

    //Ejecuta el servidor

    public void run() {
        //para crear Thread según sea necesario,
        //pero volverá a utilizar hilos ya construidos cuando esten disponibles
        ExecutorService executor = Executors.newCachedThreadPool();

        pool = (ThreadPoolExecutor) executor;
        try {

            //ejecuta hilo que imprime las estadisticas de creacion de hilos
            executor.submit(new ClientsThread());

            //Socket de servidor para esperar peticiones de la red
            ServerSocket serverSocket = new ServerSocket(PORT);
            System.out.println("Server> Servidor iniciado");
            while (true) {
                try {
                    System.out.println();

                    //en espera de conexion, si existe la acepta
                    Socket clientSocket = serverSocket.accept();

                    //crea un nuevo hilo para la petición
                    executor.submit(new ClientThread(clientSocket, ++idThread));
                } catch (IOException ex) {//error
                    System.err.println(ex.getMessage());
                }
            }
        } catch (IOException ex) {
            System.err.println(ex.getMessage());
        }
    }



    //Hilo que imprime la cantidad de hilos activos y ejecutados en la aplicación
    //restamos 1 para no tomar en cuenta este mismo hilo
    public class ClientsThread implements Runnable {

        @Override
        public void run() {
            if(pool!=null)
                while (true) {
                    try {
                        Thread.sleep(6000);//6 segundos
                    } catch (InterruptedException ex) {
                        System.err.println(ex.getMessage());
                    }
                    JSystem.out.printColorln(JSystem.Color.blue,"Server> Thread en ejecución: (" + (pool.getActiveCount()-1) + ") Thread generados: ("+(pool.getTaskCount()-1)+")");
                }
        }

    }

    public static void main(String[] args) {
        MainServer server = new MainServer();
        server.run();
    }

}