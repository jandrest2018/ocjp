package cliente;

import helper.Helper;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;


public class MainClient {


    public static void main(String[] args) {
        ExecutorService executor = Executors.newCachedThreadPool();
        while (true) {

            //duerme el hilo del cliente unos segundos antes de generar una nueva peticion
            Helper.fn.sleep();

            //nuevo hilo
            executor.submit(new ServerThread());
        }
    }

}