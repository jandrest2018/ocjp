package cliente;
import helper.Helper;
import java.io.IOException;
import java.io.InputStream;
import java.io.PrintStream;
import java.net.Socket;

public class ServerThread implements Runnable {

    //puerto
    private final static int PORT = 5000;

    //host
    private final static String SERVER = "localhost";

    @Override
    public void run() {
        Socket clientSocket;//Socket para la comunicacion cliente servidor
        try {

            //abre socket
            clientSocket = new Socket(SERVER, PORT);

            //para imprimir datos del servidor
            PrintStream toServer = new PrintStream(clientSocket.getOutputStream());

            //Para leer lo que envie el servidor
            InputStream stream = clientSocket.getInputStream();

            //genera numero aleatorio y manda al servidor
            toServer.println(Helper.fn.rndInt(2, 20));

            //lee respuesta del servidor
            byte[] bytes = new byte[256];
            stream.read(bytes, 0, 256);

            //convierte a string
            String received = new String(bytes, "UTF-8");

            //imprime en pantalla
            System.out.println("Server> recibido del servior fibonacci" + received.trim());
            clientSocket.close();
        } catch (IOException ex) {
            System.err.println(ex.getMessage());
            System.exit(0);//fin de app
        }

    }

}