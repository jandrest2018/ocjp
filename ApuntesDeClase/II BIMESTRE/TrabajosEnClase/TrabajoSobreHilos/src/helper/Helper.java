package helper;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Random;


public final class Helper {

    public Helper(){}

    public static abstract class fn {

        //retorna fecha y hora preformateada
        public static String fecha_actual(){
            SimpleDateFormat dt = new SimpleDateFormat("dd/MM/yyyy hh:mm:ss.SSS");
            return dt.format(new Date());
        }

        //Duerme el hilo unos segundos (random)
        public static void sleep() {
            Random r = new Random();
            try {
                int t = r.nextInt(6000) + 500;
                Thread.sleep(t);
            } catch (InterruptedException ex) {
                System.err.println(ex.getMessage());
            }
        }


        //Retorna un numero aleatorio entero entre un rango dado
        public static int rndInt(int min, int max){
            Random r = new Random();
            int n = r.nextInt(max-min) + min;
            return n;
        }
    }

}