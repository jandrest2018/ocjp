package Trabajo;

import java.util.Arrays;
import Trabajo.ClaseExterior.InnerClass;
import Trabajo.ClaseExterior.StaticNestedClass;

public class ClassInner {

    public static void main(String[] args) {

        //Escuela Politecnica Nacional --- TRABAJO EN CLASE --- José Andrés Tinajero
        System.out.println("TRABAJO EN CLASE DE OCJP");
        System.out.println("-Inner classes\n" +
                "-Method-local Inner Classes\n" +
                "-Anonymous Inner Classes\n" +
                "-Static Nested Classes\n");

        ClaseExterior outer = new ClaseExterior(1,2,3,4);

        //-- STATIC NESTED CLASSES --
        //Las nested classes se pueden utilizar en la importación para la fácil instanciación
        //Si la Static Nested es static, se llama Static Nested Classes. Las cuales pueden tener acceso solamente
        // a los miembros static de la clase externa.

        //La Static Nested Class es igual que cualquier otra clase de nivel superior y está anidada sólo
        // para la conveniencia del embalaje.
        StaticNestedClass staticNestedClass = new StaticNestedClass();
        StaticNestedClass staticNestedClass1 = new StaticNestedClass();

        System.out.println(staticNestedClass.getName());
        staticNestedClass.d=10;
        System.out.println(staticNestedClass.d);
        System.out.println(staticNestedClass1.d);

        //-- INNER CLASS --
        InnerClass innerClass = outer.new InnerClass();
        System.out.println(innerClass.getName());
        System.out.println(innerClass);
        innerClass.setValues();
        System.out.println(innerClass);

        //Llamada al metodo usando "Local Inner Class"
        outer.print("Exterior");

        //LLamada al metodo usando "Anonymous Inner Class"
        System.out.println(Arrays.toString(outer.getFilesInDir("src/Trabajo/nested", ".java")));
        System.out.println(Arrays.toString(outer.getFilesInDir("bin/Trabajo/nested", ".class")));
    }

}
