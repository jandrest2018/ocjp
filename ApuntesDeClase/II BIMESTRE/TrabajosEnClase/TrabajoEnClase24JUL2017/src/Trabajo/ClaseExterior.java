package Trabajo;

import java.io.File;
import java.io.FilenameFilter;

public class ClaseExterior {

    private static String name = "ClaseExterior";
    private int i;
    protected int j;
    int k;
    public int l;

    //Constructor de Clase Exterior
    public ClaseExterior(int i, int j, int k, int l) {
        this.i = i;
        this.j = j;
        this.k = k;
        this.l = l;
    }

    public int getI() {
        return this.i;
    }

    //Static Nested Class, puede acceder a variables/métodos static de ClaseExterior
    static class StaticNestedClass {
        private int a;
        protected int b;
        int c;
        public int d;

        public int getA() {
            return this.a;
        }

        public String getName() {
            return name;
        }
    }

    //Inner Class, no static y puede acceder a todas las variables/métodos de la ClaseExterior
    //Cualquier Inner Class no static se conoce como Inner Class en java. Está asociada
    // con el objeto de la clase y pueden acceder a todas las variables y métodos de la clase externa.

    //Dado que las Inner Classes están asociadas con instancia, no podemos tener ninguna variable estática
    // en ellas.
    class InnerClass {
        private int w;
        protected int x;
        int y;
        public int z;

        public int getW() {
            return this.w;
        }

        public void setValues() {
            this.w = i;
            this.x = j;
            this.y = k;
            this.z = l;
        }

        @Override
        public String toString() {
            return "w=" + w + ":x=" + x + ":y=" + y + ":z=" + z;
        }

        public String getName() {
            return name;
        }
    }

    //-- LOCAL INNER CLASS --
    //Si una clase se define dentro de un método, se conoce como Local Inner Class
    //Dado que la Local Inner Class no está asociada con Object, no podemos
    // usar modificadores de acceso privados, públicos o protegidos con él.
    // Los únicos modificadores permitidos son abstractos o finales.

    //Una Local Inner Class puede acceder a todos los miembros de la clase de inclusión
    // y las variables finales locales en el ámbito definido.
    public void print(String initial) {
        //Local Inner Class dentro del metodo
        class Logger {
            String name;

            public Logger(String name) {
                this.name = name;
            }

            public void log(String str) {
                System.out.println(this.name + ": " + str);
            }
        }

        Logger logger = new Logger(initial);
        logger.log(name);
        logger.log("" + this.i);
        logger.log("" + this.j);
        logger.log("" + this.k);
        logger.log("" + this.l);
    }

    //-- ANONYMOUS INNER CLASS --
    //Una Anonymous Inner Class se define e instancia en una única sentencia.
    //La Anonymous Inner Class siempre extiende una clase o implementa una interface.
    // Dado que una Anonymous Inner Class no tiene nombre, no es posible definir un constructor.
    //Son accesibles sólo en el punto donde se define.
    public String[] getFilesInDir(String dir, final String ext) {
        File file = new File(dir);
        //anonymous inner class implementando la interface FilenameFilter
        String[] filesList = file.list(new FilenameFilter() {

            @Override
            public boolean accept(File dir, String name) {
                return name.endsWith(ext);
            }

        });
        return filesList;
    }
}