package clases;

import java.util.*;

/**
 * Created by Black on 5/7/2017.
 */
public class Main {
    public static void main(String[] args){

        List<String> sl = new ArrayList<String>();
        sl.add("hola");
        sl.add("casa");
        sl.add("33");

        static void mostrarTodos(Collection c){
            Iterator it = c.iterator();
            while (it.hasNext()){
                System.out.println(it.next() + " ");
            }
        }

        mostrarTodos(sl);

        Set hashSet = new HashSet();
        hashSet.add("set1");
        hashSet.add("set2");
        hashSet.add("set3");
        hashSet.add("set4");
        hashSet.add("set4");
        hashSet.add("set4");

        mostrarTodos(hashSet);

        SortedSet treeSet = new TreeSet();
        treeSet.add("tsert1");
        treeSet.add("tsert2");
        treeSet.add("tsert3");
        treeSet.add("tsert4");
        treeSet.add("tsert1");
        treeSet.add("tsert1");

        mostrarTodos(treeSet);

        Map map = new HashMap();
        map.put("k1","v1");
        map.put("k5","v5");
        map.put("k3","v3");
        map.put("k4","v4");
        map.put("k2","v2");
        map.put("k6","v6");

        mostrarTodos(map.keySet());
        mostrarTodos(map.values());

        Map map2 = new TreeMap();
        map2.put("k1","v1");
        map2.put("k5","v5");
        map2.put("k3","v3");
        map2.put("k2","v2");
        map2.put("k4","v4");
        map2.put("k6","v6");

        mostrarTodos(map2.keySet());
        mostrarTodos(map2.values());

    }
}
