package Prueba4;

class ColorException extends Exception {}
class WhiteException extends ColorException{}
abstract class Color{
    abstract void m1() throws ColorException;
}
class White extends Color{
    void m1() throws WhiteException{throw new WhiteException();}
    public static void main(String[] args){
        White white = new White();
        int a, b , c,d;
        a = b = c = d = 0;
        try{
            white.m1();
            a++;
        } catch(WhiteException e){b++;}
        finally {c++;}
        System.out.print(a+","+b+","+c);
    }
}