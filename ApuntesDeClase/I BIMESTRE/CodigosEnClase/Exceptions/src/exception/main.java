package exception;

/**
 * Created by Black on 7/6/2017.
 */
public class main {

    public static void main(String[] args) {
        try{
            System.out.println("Provocando un error en 3, 2, 1...");
            throw new MiExcepcion();
        }catch(MiExcepcion ex){
            System.out.println(ex.excErrorPersonalizado());
        }
    }
}
