package company;

/**
 * Created by Black on 10/5/2017.
 */
public enum Cartas {
    dimante(true), corazon(true), trebol(false), espada(true);

    private boolean roja;

    Cartas(boolean b){
        roja = b;
    }

    @Override
    public String toString(){

        String s = name();

        if (roja){
            s += "es rojo";
        }else{
            s += "es negro";
        }
        return s;

    }
}
